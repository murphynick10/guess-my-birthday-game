from random import randint
num = 0

name = input("What is your name? ")

while num < 4:

  year = randint(1924, 2004)
  month = randint(1, 12)

  print("Hello " + name + " were you born in " + str(month) + "/" + str(year) + "?")

  response = input("yes or no? ")

  if response == "no" and num < 3:
    num += 1
    print("Drat! Lemme try again!")
  if response == "no" and num == 3:
    num += 1
    print("I have other things to do. Good bye.")
  if response == "yes":
    print("I knew it!")
    num += 4
